---
title: "Minha Trajetoria"
date: 2022-09-26T19:33:34-03:00
draft: false
---

# Aprovação USP - Escola Politécnica
## Dezembro 2019
Após um ano de cursinho, consegui a aprovação na primeira lista, para estudar engenharia mecatrônica na Escola Politécnica da Universidade de São Paulo

# Primeira Iniciação Científica
## Janeiro 2021
Início da minha primeira iniciação científica que tinha como objetivo encontrar uma rota de preparação metralográfica sem tratamentos térmicos para análisar domínios magnéticos

# Aprovação Thunderatz
## Setembro 2021
Fui aceito no processo seletivo para entrar na equipe de robótica competitiva da Escola Politécnica da Universidade de São Paulo, lá participei de diversos projetos na área de robótica

# Segunda Iniciação Científica
## Janeiro 2022
Início da minha segunda iniciação científica, com o objetivo de analisar as  melhores topologias em computadores quânticos reais, através de simulações em Python com a biblioteca Qiskit

