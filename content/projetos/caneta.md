---
title: "Caneta Digital-Twin"
date: 2022-10-02T18:51:18-03:00
draft: false
---

# O que é?

Esse projeto consiste em ligar um sensor IMU em uma caneta, e transmitir a rotação e movimentação da caneta para um servidor web através de websockets

<!--more-->

# Como foi feito?

O sensor IMU foi ligado a um raspberry PI, que, abria um cliente websocket com nodeJS que se comunicava com os clientes programados em Javascript. Dessa forma era possivel ver a movimentação da caneta em qualquer local ligado em minha rede doméstica. Além disso uma imgam 3d da caneta feita com a biblioteca Three.js se movimentava copiando os movimentos lidos pelo sensor