---
title: "Perse"
date: 2022-10-02T18:51:18-03:00
draft: false
---

# O que é?

![perse-image]({{< perseURL >}})

Perse é um robô 100% autônomo que compete em competições de robótica na categoria trekking 

<!--more-->

O objetivo da categoria é de encontrar 3 pontos em um campo grande, a partir das coordenadas relativas dos ponto, além disso, também é possível adicionar cones nos locais dos pontos para auxiliar na localização.


# Como ele funciona

Perse possui diversos sensores que possibilitam a ele calcular sua posição aproximada. Ele possui um encoder em sua roda fronteira e uma IMU, que calculam a posição do robô através de odometria. Além disso, quando o robô está proximo a um cone, ele ativa uma webcam, cuja imagem vista é analisada por uma inteligencia artificial treinada para encontrar cones, dessa forma o Perse pode acertar sua trajetória e chegar em seu objetivo. Além disso o robô possui uma espécie de botão em sua frente, que, quando encosta no cone permite o robô saber que chegou em seu objetivo